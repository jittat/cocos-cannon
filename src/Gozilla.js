var Gozilla = cc.Sprite.extend({
  ctor: function() {
    this._super();
    this.initWithFile('images/gozilla.png');
    this.setAnchorPoint( new cc.Point( 0.5, 0.4 ) );
  }
});
