var PowerBar = cc.Sprite.extend({
  ctor: function() {
    this._super();
    this.initWithFile( 'images/powerbar.png' );
    this.setAnchorPoint( new cc.Point( 0, 0 ) );
    this._currentScale = 0;
  },

  start: function( s ) {
    this._currentScale = s;
    this._direction = 1;
    this.setScale( s, 1 );
    this.setVisible( true );
    this.schedule( this.update, 0.1 );
  },

  stop: function() {
    this.unschedule( this.update );
  },

  update: function() {
    if ( ( this._currentScale + this._direction > 10 ) ||
         ( this._currentScale + this._direction <= 0 ) )
      this._direction = - this._direction;
    this._currentScale += this._direction;
    this.setScale( this._currentScale, 1 );
  },

  getPowerScale: function() {
    return this._currentScale;
  }
});
