var Cannon = cc.Sprite.extend({
  _angle: 0,

  ctor: function() {
    this._super();
    this.initWithFile('images/cannon.png');
    this.setAnchorPoint( new cc.Point( 0.5, 0.3 ) );
  },

  turnLeft: function() {
    this._angle -= 5;
    this.setRotation( this._angle );
  },

  turnRight: function() {
    this._angle += 5;
    this.setRotation( this._angle );
  },

  getVelocity: function() {
    var nv = 1,
        rad = this._angle * Math.PI / 180.0;
    
    return { 
      x: nv * Math.sin( rad ), 
      y: nv * Math.cos( rad ) 
    };
  }
});
