COLLISION_GAP = 16;
BALLS_PER_MONSTER = 3;

var GameLayer = cc.LayerColor.extend({
  _ballCount: 0,
  _score: 0,
  _cannonPower: 0,

  _cannonBall: null,
  _cannon: null,
  _gozilla: null,
  _scoreLabel: null,
  _ballCountLabel: null,
  _powerBar: null,
  _powerBarStarted: false,

  init: function()
  {
    this._super( new cc.Color4B( 127, 127, 255, 255 ) );
    this.setPosition( new cc.Point( 0, 0 ) );

    this.setKeyboardEnabled(true);
    this.scheduleUpdate();

    this.createObjects();

    this.resetBallCount();

    return true;
  },

  update: function( dt ) {
    this.checkCollision();
    if ( this._cannonBall )
      this._cannonBall.update( dt );
  },

  onKeyDown: function( e ) {
    if ( e == cc.KEY.left )
      this._cannon.turnLeft();
    else if ( e == cc.KEY.right )
      this._cannon.turnRight();
    else if ( e == cc.KEY.space ) {
      if ( !this._cannonBall && 
           ( this._ballCount > 0 ) && 
           ( !this._powerBarStarted ) )
        this.startPowerBar();
    }
  },

  onKeyUp: function( e ) {
    if ( e == cc.KEY.space ) {
      if ( !this._cannonBall && ( this._ballCount > 0 ) ) 
        this.fireCannonBall();
    }
  },

  createObjects: function() {
    this._cannon = new Cannon();
    this.addChild( this._cannon );
    this._cannon.setPosition( new cc.Point( 100, 100 ) );

    this._gozilla = new Gozilla();
    this.addChild( this._gozilla );
    this.randomGozillaPosition();

    this._scoreLabel = cc.LabelTTF.create( '0', 'Arial', 40 );
    this.addChild(this._scoreLabel);
    this._scoreLabel.setPosition( new cc.Point( 700,550 ) );

    this._ballCountLabel = cc.LabelTTF.create( '0', 'Arial', 40 );
    this.addChild(this._ballCountLabel);
    this._ballCountLabel.setPosition( new cc.Point( 100,550 ) );

    this._powerBar = new PowerBar();
    this._powerBar.setPosition( new cc.Point( 150, 550 ) );
    this._powerBar.setVisible( false );
    this.addChild( this._powerBar );
  },

  updateBallCount: function( count ) {
    this._ballCount = count;
    this._ballCountLabel.setString( count );
  },

  resetBallCount: function() {
    this.updateBallCount( BALLS_PER_MONSTER );
  },

  increaseScore: function() {
    this._score += 1;
    this._scoreLabel.setString( this._score );
  },

  randomGozillaPosition: function() {
    this._gozilla.setPosition( 300 + Math.floor( Math.random() * 400 ), 100 );
  },

  redisplayGozilla: function() {
    this._gozilla.setScale( 1, 1 );
    this._gozilla.setVisible( true );
    this._gozilla.setOpacity( 255 );
  },

  removeCannonBall: function() {
    this.removeChild( this._cannonBall );
    this._cannonBall = null;
    this._powerBar.setVisible( false );
  },

  startPowerBar: function() {
    this._cannonPower = 1;
    this._powerBar.start( this._cannonPower );
    this._powerBarStarted = true;
  },

  fireCannonBall: function() {
    var vel = this._cannon.getVelocity(),
        power = this._powerBar.getPowerScale();

    this._powerBar.stop();
    this._powerBarStarted = false;

    this._cannonBall = new CannonBall( vel.x * power, 
                                       vel.y * power, 
                                       0, 
                                       -0.1 );
    this.addChild( this._cannonBall );
    this._cannonBall.setPosition( new cc.Point( 100, 100 ) );
  },

  checkCollision: function() {
    if ( this.checkBallHit() ) {
      this.removeCannonBall();
      var randomAction = cc.CallFunc.create( this.randomGozillaPosition, this );
      var showAction = cc.CallFunc.create( this.redisplayGozilla, this );
      var hitAction = cc.Sequence.create([ cc.ScaleTo.create( 0.5, 5 ),
                                           cc.FadeOut.create( 0.5 ),
                                           randomAction,
                                           showAction ]);
      this._gozilla.runAction( hitAction );
      this.resetBallCount();
      this.increaseScore();
    } else
      this.checkGroundCollision();
  },

  checkBallHit: function() {
    if ( !this._cannonBall )
      return false;

    var ballPos = this._cannonBall.getPosition();
    var gozillaPos = this._gozilla.getPosition();

    return ( (Math.abs( ballPos.x - gozillaPos.x ) < COLLISION_GAP) &&
             (Math.abs( ballPos.y - gozillaPos.y ) < COLLISION_GAP) );
  },

  checkGroundCollision: function() {
    if ( this._cannonBall ) {
      var ball = this._cannonBall,
          p = ball.getPosition();
      
      if ( p.y < 100 ) {
        this.removeCannonBall();
        this.updateBallCount( this._ballCount - 1 );
      }
    }
  }
});

var CannonScene = cc.Scene.extend({
  onEnter: function () {
    this._super();
    var layer = new GameLayer();
    layer.init();
    this.addChild( layer );
  }
});
