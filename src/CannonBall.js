var CannonBall = cc.Sprite.extend({
  _vy: 0,
  _vx: 0,
  _ax: 0,
  _ay: 0,

  ctor: function( vx, vy, ax, ay ) {
    this._super();
    this.initWithFile('images/ball.png');

    this._vx = vx;
    this._vy = vy;
    this._ax = ax;
    this._ay = ay;
  },

  update: function() {
    var currentPosition = this.getPosition();

    this._vx += this._ax;
    this._vy += this._ay;
    this.setPosition( currentPosition.x + this._vx, 
                      currentPosition.y + this._vy );

    
  }
});
